<%--
  Created by IntelliJ IDEA.
  User: imac
  Date: 30.08.2016
  Time: 13:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ragister page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

</head>
<body>
<a href="/main" class="btn btn-success">HOME</a>


<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title text-center">
                        <hr/>
                        <hr/>
                        <%
                            if (request.getAttribute("errors") != null){

                        %>
                        <fieldset>
                            <legend>Errors</legend>
                            <ul>
                                <% if (request.getAttribute("login_error") != null) { %>
                                <li class="error">Login error - please fill field</li>
                                <% } %>

                                <% if (request.getAttribute("firstName_error") != null) { %>
                                <li class="error">First name error - please fill field</li>
                                <% } %>

                                <% if (request.getAttribute("lastName_error") != null) { %>
                                <li class="error">Last name error - please fill field</li>
                                <% } %>

                                <% if (request.getAttribute("email_error") != null) { %>
                                <li class="error">Email error - please fill field</li>
                                <% } %>

                                <% if (request.getAttribute("password_error") != null) { %>
                                <li class="error">Password error - please fill field</li>
                                <% } %>
                            </ul>
                        </fieldset>
                        <%
                            }
                        %>

                        <h3 class="title">Account register</h3>

                    </div>
                </div>
                <div class="panel-body">
                    <form action="RegisterUser" method="post">
                        <fieldset>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input class="form-control" placeholder="Login" name="login" type="text" value="<%= request.getAttribute("login")%>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i></span>
                                    <input class="form-control" placeholder="First Name" name="first_name" type="text" value="<%= request.getAttribute("first_name")%>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-globe" aria-hidden="true"></i></span>
                                    <input class="form-control" placeholder="Last Name" name="last_name" type="text" value="<%= request.getAttribute("last_name")%>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                    <input class="form-control" placeholder="e-mail" name="e_mail" type="text" value="<%= request.getAttribute("e_mail")%>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <input class="form-control" placeholder="Password" name="password" type="password"  value="<%= request.getAttribute("password")%>">
                                </div>
                            </div>

                            <input class="btn btn-success btn-block" type="submit" value="Register">
                            <input class="btn btn-danger btn-block" type="reset" value="Reset">
                        </fieldset>


                </div>
            </div>
        </div>
    </div>
    </form>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="web/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>