<%@ page import="model.User" %>
<%@ page import="java.util.List" %>
<%@ page import="model.Training" %>
<%@ page import="controllers.AddTrainingPage" %>
<%@ page import="service.AddTrainingService" %>
<%@ page import="service.EditTrainingService" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Training Diary !</title>

    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="bootstrap/css/css.css" rel="stylesheet">
    <link href="bootstrap/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <%
        User user = (User) session.getAttribute("user");
        Training t = (Training) session.getAttribute("t");
    %>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/HomePage">Training Diary</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <%=user.getLogin()%> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile /TODO</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox /TODO</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings /TODO</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="/AddTraining"><i class="fa fa-fw fa-dashboard"></i> Add Training </a>
                </li>
                <li>
                    <a href="/editTraining"><i class="fa fa-fw fa-bar-chart-o"></i> Edit Training </a>
                </li>
                <li>
                    <a href="/history"><i class="fa fa-fw fa-wrench"></i> History of training</a>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo" class="collapse">
                        <li>
                            <a href="#">Dropdown Item</a>
                        </li>
                        <li>
                            <a href="#">Dropdown Item</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">

                    <h1 class="page-header">
                        Workout
                        <small>Welcome you <%= user.getFirstName() + " " + user.getLastName()%></small>
                    </h1>
                    <form action="/ActionOnTraining" method="post">
                <%
                    EditTrainingService editTrainingService = new EditTrainingService();
                    int id = Integer.parseInt(request.getParameter("trainingId"));
                    request.getSession().setAttribute("id",id);
                    List<Training> tList = editTrainingService.getTrainingById(id);
                    for (Training ttr : tList){
                %>
                   Name of training is :  <%=ttr.getNameTrain()%>
                    Id : <%=ttr.getId()%>

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="panel panel-primary dropdown">
                            <div class="panel-heading">
                                <h1><%=ttr.getNameTrain()%> </h1>
                            </div>
                            <!--legs -->
                            <div class="panel-body">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <h4>Legs</h4>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <div class="row">
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <%=ttr.getLegsEx()%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 1" name="legs1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 2" name="legs2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 3" name="legs3">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 4" name="legs4">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="weight" name="weightLegs">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </ul>
                            </div>
                        </div>
                        <!-- Bic -->
                        <div class="panel panel-primary dropdown">
                            <div class="panel-body">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <h4>Biceps</h4>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <div class="row">
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <%=ttr.getBicepsEX()%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 1" name="bic1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 2" name="bic2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 3" name="bic3">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 4" name="bic4">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="weight" name="weightBic">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </ul>
                            </div>
                        </div>
                        <!-- Triceps -->
                        <div class="panel panel-primary dropdown">
                            <div class="panel-body">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <h4>Triceps</h4>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <div class="row">
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <%=ttr.getTricepsEX()%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 1" name="tric1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 2" name="tric2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 3" name="tric3">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 4" name="tric4">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="weight" name="weightTric">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </ul>
                            </div>
                        </div>
                        <!-- Back -->
                        <div class="panel panel-primary dropdown">
                            <div class="panel-body">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <h4>Back</h4>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <div class="row">
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <%=ttr.getBackEX()%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 1" name="back1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 2" name="back2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 3" name="back3">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 4" name="back4">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="weight" name="weightBack">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </ul>
                            </div>
                        </div>
                        <!-- Chest -->
                        <div class="panel panel-primary dropdown">
                            <div class="panel-body">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <h4>Chest</h4>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <div class="row">
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <%=ttr.getChestEX()%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 1" name="chest1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 2" name="chest2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 3" name="chest3">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 4" name="chest4">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="weight" name="weightChest">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </ul>
                            </div>
                        </div>
                        <!-- ABS -->
                        <div class="panel panel-primary dropdown">
                            <div class="panel-body">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <h4>ABS</h4>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <div class="row">
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <%=ttr.getAbsEX()%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 1" name="abs1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 2" name="abs2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 3" name="abs3">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 4" name="abs4">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="weight" name="weightABS">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </ul>
                            </div>
                        </div>
                        <!-- Forearm -->
                        <div class="panel panel-primary dropdown">
                            <div class="panel-body">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <h4>Forearm</h4>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <div class="row">
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <%=ttr.getForearmEX()%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 1" name="forearm1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 2" name="forearm2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 3" name="forearm3">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="seria 4" name="forearm4">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input class="form-control" type="number" value="" placeholder="weight" name="weightForearm">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </ul>
                            </div>
                        </div>

                    </div>
                    <!-- /.row -->



                        <%
                            }
                        %>

                    </div>
                        <input class="btn btn-success btn-block" type="submit" value="Add workout">
                        <input class="btn btn-danger btn-block" type="reset" value="Reset">
                    </form>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="bootstrap/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
