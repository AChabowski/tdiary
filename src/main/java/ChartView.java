import controllers.ViewHistoryTrainingPage;
import model.ActionTraining;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.List;

@ManagedBean
public class ChartView implements Serializable {

    private LineChartModel zoomModel;



    @PostConstruct
    public void init() {
        createZoomModel();
    }

    public LineChartModel getZoomModel() {
        return zoomModel;
    }

    private void createZoomModel() {
        zoomModel = initLinearModel();
        zoomModel.setTitle("Zoom");
        zoomModel.setZoom(true);
        zoomModel.setLegendPosition("e");
        Axis yAxis = zoomModel.getAxis(AxisType.Y);
        yAxis.setLabel("Value in kg");
        yAxis.setMin(0);
        yAxis.setMax(9000);
    }

    private LineChartModel initLinearModel() {
        LineChartModel model = new LineChartModel();

        LineChartSeries series1 = new LineChartSeries();
        LineChartSeries series2 = new LineChartSeries();
        LineChartSeries series3 = new LineChartSeries();
        LineChartSeries series4 = new LineChartSeries();
        LineChartSeries series5 = new LineChartSeries();
        LineChartSeries series6 = new LineChartSeries();
        LineChartSeries series7 = new LineChartSeries();

        series1.setLabel("ABS");
        series2.setLabel("Chest");
        series3.setLabel("Back");
        series4.setLabel("Legs");
        series5.setLabel("Forearms");
        series6.setLabel("Triceps");
        series7.setLabel("Biceps");

        model.addSeries(series1);
        model.addSeries(series2);
        model.addSeries(series3);
        model.addSeries(series4);
        model.addSeries(series5);
        model.addSeries(series6);
        model.addSeries(series7);

        ViewHistoryTrainingPage viewHistoryTrainingPage = new ViewHistoryTrainingPage();
        List<ActionTraining> wyniki = viewHistoryTrainingPage.listForChart;
        int c = 0;
        for (ActionTraining w : wyniki) {
            c++;

            series1.set(c,w.getValueABS());
            series2.set(c,w.getValueChest());
            series3.set(c,w.getValueBack());
            series4.set(c,w.getValueLegs());
            series5.set(c,w.getValueForearms());
            series6.set(c,w.getValueTriceps());
            series7.set(c,w.getValueBiceps());
        }
        return model;
    }
}
