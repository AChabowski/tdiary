package controllers;

import model.ActionTraining;
import service.AddActionTrainingService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.lang.System.out;

/**
 * Created by imac on 01.09.2016.
 */
@WebServlet(name = "ActionOnTraining", urlPatterns = ("/ActionOnTraining"))
public class ActionOnTraining extends HttpServlet {



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



        ActionTraining aT = new ActionTraining();


        Long idTraniing = EditTrainingPage.idTraining;
        aT.setIdTraining(idTraniing);
        System.out.println("ID TRENINGU Wpisanego do tabeli AC : " + idTraniing);


        //1.LEGS
        Long legs1 = Long.parseLong(request.getParameter("legs1"));
        Long legs2 = Long.parseLong(request.getParameter("legs2"));
        Long legs3 = Long.parseLong(request.getParameter("legs3"));
        Long legs4 = Long.parseLong(request.getParameter("legs4"));
        Long weightLegs = Long.parseLong(request.getParameter("weightLegs"));
        Long valueLegs = (legs1 + legs2 + legs3 + legs4 ) * weightLegs;
        System.out.println("OBWÓD NOG: " + valueLegs + "kg");
        aT.setValueLegs(valueLegs);

        //2.BICEPSE
        Long bic1 = Long.parseLong(request.getParameter("bic1"));
        Long bic2 = Long.parseLong(request.getParameter("bic2"));
        Long bic3 = Long.parseLong(request.getParameter("bic3"));
        Long bic4 = Long.parseLong(request.getParameter("bic4"));
        Long weightBic = Long.parseLong(request.getParameter("weightBic"));
        Long valueBiceps = (bic1 + bic2 + bic3 + bic4) * weightBic;
        System.out.println("OBWÓD BICA "  + valueBiceps);
        aT.setValueBiceps(valueBiceps);

        //3.Triceps
            Long tric1 = Long.parseLong(request.getParameter("tric1"));
            Long tric2 = Long.parseLong(request.getParameter("tric2"));
            Long tric3 = Long.parseLong(request.getParameter("tric3"));
            Long tric4 = Long.parseLong(request.getParameter("tric4"));
            Long weightTric = Long.parseLong(request.getParameter("weightTric"));
            Long valueTriceps = (tric1 + tric2 + tric3 + tric4) * weightTric;
            System.out.println("OBWÓD TRICA " + valueTriceps);
            aT.setValueTriceps(valueTriceps);

            //4.Back
            Long back1 = Long.parseLong(request.getParameter("back1"));
            Long back2 = Long.parseLong(request.getParameter("back2"));
            Long back3 = Long.parseLong(request.getParameter("back3"));
            Long back4 = Long.parseLong(request.getParameter("back4"));
            Long weightBack = Long.parseLong(request.getParameter("weightBack"));
            Long valueBack = (back1+back2+back3+back4) * weightBack;
            System.out.println("OBWÓD BACK " + valueBack);
            aT.setValueBack(valueBack);

            //5.Chest
            Long chest1 = Long.parseLong(request.getParameter("chest1"));
            Long chest2 = Long.parseLong(request.getParameter("chest2"));
            Long chest3 = Long.parseLong(request.getParameter("chest3"));
            Long chest4 = Long.parseLong(request.getParameter("chest4"));
            Long weightChest = Long.parseLong(request.getParameter("weightChest"));
            Long valueChest = (chest1+chest2+chest3+chest4) * weightChest;
            System.out.println("OBWÓD CHEST " + valueChest);
            aT.setValueChest(valueChest);

            //6.ABS
            Long abs1 = Long.parseLong(request.getParameter("abs1"));
            Long abs2 = Long.parseLong(request.getParameter("abs2"));
            Long abs3 = Long.parseLong(request.getParameter("abs3"));
            Long abs4 = Long.parseLong(request.getParameter("abs4"));
            Long weightABS = Long.parseLong(request.getParameter("weightABS"));
            Long valueABS = (abs1+abs2+abs3+abs4) * weightABS;
            System.out.println("OBWÓD ABS " + valueABS);
            aT.setValueABS(valueABS);

            //7.Forearms
            Long forearm1 = Long.parseLong(request.getParameter("forearm1"));
            Long forearm2 = Long.parseLong(request.getParameter("forearm2"));
            Long forearm3 = Long.parseLong(request.getParameter("forearm3"));
            Long forearm4 = Long.parseLong(request.getParameter("forearm4"));
            Long weightForearm = Long.parseLong(request.getParameter("weightForearm"));
            Long valueForearms = (forearm1+forearm2+forearm3+forearm4) * weightForearm;
            System.out.println("OBWÓD FOREARM " + valueForearms);
            aT.setValueForearms(valueForearms);




        try {
            AddActionTrainingService addActionTrainingService = new AddActionTrainingService();
            boolean result =  addActionTrainingService.registerActionTraining(aT);

            if (result){
                response.sendRedirect("/HomePage");
                out.println("Action - Training has been added");
            } else {
                out.println("<h1>Registration Failed</h1>");
            }
        } finally {
            out.close();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



        RequestDispatcher view = request.getRequestDispatcher("views/actionTraining.jsp");
        view.forward(request,response);



    }
}
