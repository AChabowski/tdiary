package controllers;

import service.EditTrainingService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by imac on 04.09.2016.
 */
@WebServlet(name = "HistoryTrainingPage",urlPatterns = ("/history"))
public class HistoryTrainingPage extends HttpServlet {


    public static Long idtraining = (long) 0;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String idT = request.getParameter("idT");

        EditTrainingService editTrainingService = new EditTrainingService();
        HistoryTrainingPage.idtraining = editTrainingService.geTrainingId(idT);

        response.sendRedirect("/viewHistory?trainingId="+idT);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher("views/historyTraining.jsp");
        view.forward(request,response);


    }
}
