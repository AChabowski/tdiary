package controllers;

import model.User;
import service.LoginService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "HomePage", urlPatterns = ("/HomePage"))
public class HomePage extends HttpServlet {

    public static String login = "";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String login = request.getParameter("login");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();
        boolean result = loginService.authenticateUser(login,password);
        User user = loginService.getUserById(login);
        if(result == true){

            HomePage.login = String.valueOf(loginService.getUserById(login));
            System.out.println("Przypisanie uzytkownik " + HomePage.login);

            request.getSession().setAttribute("user",user);
            response.sendRedirect("/HomePage");
        } else {
            response.sendRedirect("/main");

        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        RequestDispatcher view = request.getRequestDispatcher("views/homePage.jsp");
        view.forward(request,response);
    }
}
