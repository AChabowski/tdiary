package controllers;

import model.User;
import service.RegisterService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "RegisterUser", urlPatterns = ("/RegisterUser"))
public class RegisterUser extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("errors",false);
        User u = new User();

        String login = request.getParameter("login");
        System.out.println(login);
        if (login.length() == 0){
            request.setAttribute("errors",true);
            request.setAttribute("login_error",true);
            request.setAttribute("login","");
        } else {
            u.setLogin(login);
            request.setAttribute("login",login);
        }


        String firstName =request.getParameter("first_name");
        System.out.println(firstName);
        if (firstName.length() == 0){
            request.setAttribute("errors",true);
            request.setAttribute("firstName_error",true);
            request.setAttribute("first_name","");
        } else {
            u.setFirstName(firstName);
            request.setAttribute("first_name",firstName);
        }

        String lastName = request.getParameter("last_name");
        System.out.println(lastName);
        if (lastName.length() == 0){
            request.setAttribute("errors",true);
            request.setAttribute("lastName_error",true);
            request.setAttribute("last_name","");
        } else {
            u.setLastName(lastName);
            request.setAttribute("last_name",lastName);
        }

        String email = request.getParameter("e_mail");
        System.out.println(email);
        if (email.length() == 0){
            request.setAttribute("errors",true);
            request.setAttribute("email_error",true);
            request.setAttribute("e_mail","");
        } else {
            u.setEmail(email);
            request.setAttribute("e_mail",email);
        }

        String password = request.getParameter("password");
        System.out.println(password);
        if (password.length() == 0 ){
            request.setAttribute("errors",true);
            request.setAttribute("password_error",true);
            request.setAttribute("password","");
        } else {
            u.setPassword(password);
            request.setAttribute("password",password);
        }

        if ((Boolean)request.getAttribute("errors")){

            RequestDispatcher view = request.getRequestDispatcher("views/register.jsp");
            view.forward(request,response);
        } else {
            PrintWriter out = response.getWriter();

            try {
                RegisterService registerService = new RegisterService();
                boolean result = registerService.register(u);

                if (result){
                    response.sendRedirect("/main");
                    System.out.println("User has been added");
                } else {
                    out.println("<h1>Registration Failed</h1>");
                    out.println("To try again<a href=register.jsp>Click here</a>");
                }
            } finally {
                out.close();
            }

        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("login","");
        request.setAttribute("first_name","");
        request.setAttribute("last_name","");
        request.setAttribute("e_mail","");
        request.setAttribute("password","");

        RequestDispatcher view = request.getRequestDispatcher("views/register.jsp");
        view.forward(request,response);
    }
}
