package controllers;

import service.EditTrainingService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.lang.System.out;

/**
 * Created by imac on 01.09.2016.
 */
@WebServlet(name = "EditTrainingPage", urlPatterns = ("/editTraining"))
public class EditTrainingPage extends HttpServlet {

   public static Long idTraining = (long)0;


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String idT = request.getParameter("idT");

        EditTrainingService editTrainingService = new EditTrainingService();
        EditTrainingPage.idTraining = editTrainingService.geTrainingId(idT);


        response.sendRedirect("/ActionOnTraining?trainingId="+idT);
        out.println("Training has been added with id: " + idTraining);



    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



        RequestDispatcher view = request.getRequestDispatcher("views/editTraining.jsp");
         view.forward(request,response);

    }
}
