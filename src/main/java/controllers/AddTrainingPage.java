package controllers;

import model.Training;
import service.AddTrainingService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by imac on 01.09.2016.
 */
@WebServlet(name = "AddTrainingPage", urlPatterns = ("/AddTraining"))
public class AddTrainingPage extends HttpServlet {



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("errors",false);
        request.setAttribute("addTraining",false);

        Training t = new Training();

        String login = HomePage.login;
        t.setLoginUser(login);

        String nameTrain = request.getParameter("training-name");
        if (nameTrain.length() == 0 ){
            request.setAttribute("errors", true);
            request.setAttribute("nameTrain_error",true);
            request.setAttribute("training-name","");
        } else {
            t.setNameTrain(nameTrain);
            request.setAttribute("training-name",nameTrain);
        }


        String legsEX = request.getParameter("LegsEX");
        if (legsEX.length() == 0) {
            request.setAttribute("errors",true);
            request.setAttribute("legsEX_error",true);
            request.setAttribute("LegsEX","");
        } else {
            t.setLegsEx(legsEX);
            request.setAttribute("LegsEX",legsEX);
        }


        String forearmEX = request.getParameter("ForearmEX");
        if (forearmEX.length() == 0 ){
            request.setAttribute("errors",true);
            request.setAttribute("forearmEX_error",true);
            request.setAttribute("ForearmEX","");
        } else {
            t.setForearmEX(forearmEX);
            request.setAttribute("ForearmEX",forearmEX);
        }

        String bicepsEX = request.getParameter("BicepsEX");
        if (bicepsEX.length() == 0){
            request.setAttribute("errors",true);
            request.setAttribute("bicepsEX_error",true);
            request.setAttribute("BicepsEX","");
        } else {
            t.setBicepsEX(bicepsEX);
            request.setAttribute("BicepsEX",bicepsEX);
        }

        String tricepsEX = request.getParameter("TricepsEX");
        if (tricepsEX.length() == 0){
            request.setAttribute("errors",true);
            request.setAttribute("tricepsEX_error",true);
            request.setAttribute("TricepsEX","");
        } else {
            t.setTricepsEX(tricepsEX);
            request.setAttribute("TricepsEX",tricepsEX);
        }

        String backEX = request.getParameter("BackEX");
        if (backEX.length() == 0){
            request.setAttribute("errors",true);
            request.setAttribute("backEX_error",true);
            request.setAttribute("BackEX","");
        } else {
            t.setBackEX(backEX);
            request.setAttribute("BackEX",backEX);
        }

        String chestEX = request.getParameter("ChestEX");
        if (chestEX.length() == 0){
            request.setAttribute("errors",true);
            request.setAttribute("chestEX_error",true);
            request.setAttribute("ChestEX","");
        } else {
            t.setChestEX(chestEX);
            request.setAttribute("ChestEX",chestEX);
        }

        String absEX = request.getParameter("AbsEX");
        if (absEX.length() == 0){
            request.setAttribute("errors",true);
            request.setAttribute("absEX_error",true);
            request.setAttribute("AbsEX","");
        } else {
            t.setAbsEX(absEX);
            request.setAttribute("AbsEX",absEX);
        }


        if ((Boolean) request.getAttribute("errors")){
            RequestDispatcher view = request.getRequestDispatcher("views/addTraining.jsp");
            view.forward(request,response);
        } else {
            PrintWriter out = response.getWriter();

            try {
                AddTrainingService addTrainingService = new AddTrainingService();
                boolean result = addTrainingService.registerTraining(t);

                if (result) {
                    request.setAttribute("addTraining",true);
                    request.setAttribute("NameOfTraining",nameTrain);
                    RequestDispatcher view = request.getRequestDispatcher("views/homePage.jsp");
                    view.forward(request,response);
                } else {
                    out.println("<h1>Registration Failed</h1>");
                    out.println("To try again<a href=/addTraining>Click here</a>");
                }
            } finally {
                out.close();
            }

        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("training-name","");
        request.setAttribute("LegsEX","");
        request.setAttribute("ForearmEX","");
        request.setAttribute("BicepsEX","");
        request.setAttribute("TricepsEX","");
        request.setAttribute("BackEX","");
        request.setAttribute("ChestEX","");
        request.setAttribute("AbsEX","");

        RequestDispatcher view = request.getRequestDispatcher("views/addTraining.jsp");
        view.forward(request,response);
    }
}
