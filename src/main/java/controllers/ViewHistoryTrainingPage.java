package controllers;

import com.google.gson.Gson;
import model.ActionTraining;
import service.HistoryTrainingService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by imac on 12.09.2016.
 */
@WebServlet(name = "ViewHistoryTrainingPage", urlPatterns = "/viewHistory")
public class ViewHistoryTrainingPage extends HttpServlet {

     public static List<ActionTraining> listForChart ;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher("index.xhtml");
        view.forward(request,response);


        /// CHART JS

        HistoryTrainingService historyTrainingService = new HistoryTrainingService();
        int id = Integer.parseInt(request.getParameter("trainingId"));
        request.getSession().setAttribute("id",id);
        List<ActionTraining> atlList = historyTrainingService.getActionTrainingListById(id);
        ViewHistoryTrainingPage.listForChart = atlList;
        System.out.println("Przypisane ID w Servlecie VIEWhistoryTRAININGpage " + id);
        System.out.println("Public static ID VHTP " + listForChart);

        for (ActionTraining i : atlList) {

            Gson gson = new Gson();
            String jsonString = gson.toJson(i);
            response.setContentType("application/json");
            response.getWriter().write(jsonString);

        }

    }



}

