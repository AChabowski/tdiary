package controllers;

import service.EditTrainingService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by imac on 17.09.2016.
 */
@WebServlet(name = "DeleteTrainingPage", urlPatterns = "/delete")
public class DeleteTrainingPage extends HttpServlet {
    private static String LIST_Edit = "views/editTraining.jsp";

    public static Long idTraining = (long)0;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    String forward ="";
    String action = request.getParameter("action");

        EditTrainingService editTrainingService = new EditTrainingService();

    if (action.equalsIgnoreCase("delete")){
        int trainingId = Integer.parseInt(request.getParameter("TID"));
        editTrainingService.deleteTraining(trainingId);
        forward = LIST_Edit;
        } else {

        System.out.println("Something went wrong !!!");
    }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
