package service;

import hibernate.HibernateUtil;
import model.Training;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Created by imac on 01.09.2016.
 */
public class AddTrainingService {

    public boolean registerTraining(Training t){
        Session session = HibernateUtil.openSession();
        Transaction tx = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            session.saveOrUpdate(t);
            tx.commit();
        } catch (Exception e){
            if ( tx != null){
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }



}
