package service;

import hibernate.HibernateUtil;
import model.User;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class LoginService {

    public boolean authenticateUser(String login, String password){
        User user = getUserById(login);
        if ( login != null && user.getLogin().equals(login) && user.getPassword().equals(password) && password != null){
            return true;
        } else {
            return false;
        }
    }


    public User getUserById(String login){
        Session session = HibernateUtil.openSession();
        Transaction tx = null;
        User user = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            Query query = session.createQuery("from User where login='"+login+"'");
            user = (User)query.uniqueResult();
            tx.commit();
        } catch (Exception e) {
            if (tx != null){
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }
}
