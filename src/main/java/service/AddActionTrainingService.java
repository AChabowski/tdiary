package service;

import hibernate.HibernateUtil;
import model.ActionTraining;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Created by imac on 03.09.2016.
 */
public class AddActionTrainingService {

    // Dodawanie Workout-u do DB

    public boolean registerActionTraining(ActionTraining aT){
        Session session = HibernateUtil.openSession();
        Transaction tx = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            session.saveOrUpdate(aT);
            tx.commit();
        } catch (Exception e){
            if ( tx != null){
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }


}
