package service;

import controllers.HistoryTrainingPage;
import hibernate.HibernateUtil;
import model.ActionTraining;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imac on 12.09.2016.
 */
public class HistoryTrainingService {

    public List<ActionTraining> getActionTrainingListById(int id) {
        List<ActionTraining> aTlist = new ArrayList<ActionTraining>();
        Session session = HibernateUtil.openSession();
        Transaction tx = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            aTlist = session.createQuery("FROM ActionTraining where idTraining='" + id + "'").list();
            System.out.println("lista AC: " +aTlist);
            tx.commit();

        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return aTlist;
    }


    public long getActionTrainingId(String idTraining) {
        Session session = HibernateUtil.openSession();
        long result = 0;
        Transaction tx = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            Query query = session.createQuery("SELECT idActionTrain FROM ActionTraining where idTraining='" + idTraining + "'");
            System.out.println((query.uniqueResult()).toString());
            result = Long.parseLong((query.uniqueResult()).toString());
            System.out.println(result);
            tx.commit();

        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return result;
    }
}
