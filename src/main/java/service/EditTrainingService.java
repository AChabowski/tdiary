package service;

import controllers.HomePage;
import hibernate.HibernateUtil;
import model.Training;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imac on 01.09.2016.
 */
public class EditTrainingService {

    //lista do wyswietania treningow  do wykorzystania w .jsp
    public List<Training> getListOfTrainings(){
        List<Training> tList = new ArrayList<Training>();
        Session session = HibernateUtil.openSession();
        Transaction tx = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            tList = session.createQuery("from Training where loginUser='" + HomePage.login +"'").list();
            System.out.println(tList);
            tx.commit();
        } catch (Exception e){
            if(tx != null){
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return tList;

    }

    //wybieranie trening po id do edycji/dodania zmian !!
    public List<Training> getTrainingById(int id){
        List<Training> list = new ArrayList<Training>();
        Session session = HibernateUtil.openSession();
        Transaction tx = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            list = session.createQuery("from Training where id ='" + id + "'").list();
            tx.commit();
        } catch (Exception e){
            if (tx != null){
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return list;
    }



    //!!!!!1
    public long geTrainingId(String idTraining) {
        Session session = HibernateUtil.openSession();
        long result = 0;
        Transaction tx = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            Query query = session.createQuery("SELECT id FROM Training where id='" + idTraining + "'");
            System.out.println((query.uniqueResult()).toString());
            result = Long.parseLong((query.uniqueResult()).toString());
            System.out.println(result);
            tx.commit();

        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return result;
    }


    public Training deleteTraining(int t){
        Session session = HibernateUtil.openSession();
        Transaction tx = null;
        Training training = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            Query query = session.createQuery("DELETE FROM Training where id='" + t + "'");
            query.executeUpdate();
            tx.commit();
        } catch (Exception e){
            if (tx != null){
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return training;
    }





}
