package service;


import hibernate.HibernateUtil;
import model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class RegisterService {

    public boolean register(User u){
    Session session = HibernateUtil.openSession();
    if (isUserExist(u))
        return false;

        Transaction tx = null;
        try{
            tx = session.getTransaction();
            tx.begin();
            session.saveOrUpdate(u);
            tx.commit();
        } catch (Exception e){
            if (tx != null){
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }

    public boolean isUserExist(User user){
        Session session = HibernateUtil.openSession();
        boolean result = false;
        Transaction tx = null;

        try {
            tx = session.getTransaction();
            tx.begin();
            Query query = session.createQuery("from User where login='" + user.getLogin() + "'");
            User u = (User) query.uniqueResult();
            tx.commit();
            if (u != null)
                result = true;
        } catch (Exception ex){
            if (tx != null){
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return  result;
    }
}
