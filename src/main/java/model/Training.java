package model;

import javax.persistence.*;

@Entity
@Table(name="Training")
public class Training {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String loginUser;

    private String nameTrain;
    private String legsEx;
    private String forearmEX;
    private String bicepsEX;
    private String tricepsEX;
    private String chestEX;
    private String absEX;
    private String backEX;


    public String getBackEX() {
        return backEX;
    }

    public void setBackEX(String backEX) {
        this.backEX = backEX;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(String loginUser) {
        this.loginUser = loginUser;
    }

    public String getNameTrain() {
        return nameTrain;
    }

    public void setNameTrain(String nameTrain) {
        this.nameTrain = nameTrain;
    }

    public String getLegsEx() {
        return legsEx;
    }

    public void setLegsEx(String legsEx) {
        this.legsEx = legsEx;
    }

    public String getForearmEX() {
        return forearmEX;
    }

    public void setForearmEX(String forearmEX) {
        this.forearmEX = forearmEX;
    }

    public String getBicepsEX() {
        return bicepsEX;
    }

    public void setBicepsEX(String bicepsEX) {
        this.bicepsEX = bicepsEX;
    }

    public String getTricepsEX() {
        return tricepsEX;
    }

    public void setTricepsEX(String tricepsEX) {
        this.tricepsEX = tricepsEX;
    }

    public String getChestEX() {
        return chestEX;
    }

    public void setChestEX(String chestEX) {
        this.chestEX = chestEX;
    }

    public String getAbsEX() {
        return absEX;
    }

    public void setAbsEX(String absEX) {
        this.absEX = absEX;
    }

    @Override
    public String toString() {
        return "Training{" +
                "id=" + id +
                ", loginUser='" + loginUser + '\'' +
                ", nameTrain='" + nameTrain + '\'' +
                ", legsEx='" + legsEx + '\'' +
                ", forearmEX='" + forearmEX + '\'' +
                ", bicepsEX='" + bicepsEX + '\'' +
                ", tricepsEX='" + tricepsEX + '\'' +
                ", chestEX='" + chestEX + '\'' +
                ", absEX='" + absEX + '\'' +
                ", backEX='" + backEX + '\'' +
                '}';
    }
}
