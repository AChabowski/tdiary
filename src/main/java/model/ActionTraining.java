package model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="ActionTraining")
public class ActionTraining {


    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long idActionTrain;
    private Long idTraining;

    private Long valueABS;
    private Long valueBack;
    private Long valueBiceps;
    private Long valueTriceps;
    private Long valueChest;
    private Long valueForearms;
    private Long valueLegs;
    private Long date;

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getIdTraining() {
        return idTraining;
    }

    public void setIdTraining(Long idTraining) {
        this.idTraining = idTraining;
    }

    public Long getValueABS() {
        return valueABS;
    }

    public void setValueABS(Long valueABS) {
        this.valueABS = valueABS;
    }

    public Long getValueBack() {
        return valueBack;
    }

    public void setValueBack(Long valueBack) {
        this.valueBack = valueBack;
    }

    public Long getValueBiceps() {
        return valueBiceps;
    }

    public void setValueBiceps(Long valueBiceps) {
        this.valueBiceps = valueBiceps;
    }

    public Long getValueTriceps() {
        return valueTriceps;
    }

    public void setValueTriceps(Long valueTriceps) {
        this.valueTriceps = valueTriceps;
    }

    public Long getValueChest() {
        return valueChest;
    }

    public void setValueChest(Long valueChest) {
        this.valueChest = valueChest;
    }

    public Long getValueForearms() {
        return valueForearms;
    }

    public void setValueForearms(Long valueForearms) {
        this.valueForearms = valueForearms;
    }

    public Long getValueLegs() {
        return valueLegs;
    }

    public void setValueLegs(Long valueLegs) {
        this.valueLegs = valueLegs;
    }

    public Long getIdActionTrain() {
        return idActionTrain;
    }

    public void setIdActionTrain(Long idActionTrain) {
        this.idActionTrain = idActionTrain;
    }

    @Override
    public String toString() {
        return "ActionTraining{" +
                "idActionTrain=" + idActionTrain +
                ", idTraining=" + idTraining +
                ", valueABS=" + valueABS +
                ", valueBack=" + valueBack +
                ", valueBiceps=" + valueBiceps +
                ", valueTriceps=" + valueTriceps +
                ", valueChest=" + valueChest +
                ", valueForearms=" + valueForearms +
                ", valueLegs=" + valueLegs +
                ", date=" + date +
                '}';
    }
}
