<%@ page import="model.User" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Training Diary !</title>

    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="bootstrap/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <%
        User user = (User) session.getAttribute("user");
    %>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/HomePage">Training Diary</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <%=user.getLogin()%> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile /TODO</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox /TODO</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings /TODO</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="/AddTraining"><i class="fa fa-fw fa-dashboard"></i> Add Training </a>
                </li>
                <li>
                    <a href="/editTraining"><i class="fa fa-fw fa-bar-chart-o"></i> Edit Training </a>
                </li>
                <li>
                    <a href="/history"><i class="fa fa-fw fa-wrench"></i> History of training</a>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo" class="collapse">
                        <li>
                            <a href="#">Dropdown Item</a>
                        </li>
                        <li>
                            <a href="#">Dropdown Item</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <form action="AddTraining" method=post>
                    <h1 class="page-header">
                        Add Training Page
                        <small>Welcome you <%= user.getFirstName() + " " + user.getLastName()%></small>
                    </h1>


                        <% if (request.getAttribute("errors") != null) {%>
                        <div class="alert alert-danger"><strong>Error!</strong>
                            <ul>
                            <% if (request.getAttribute("nameTrain_error") != null){%>
                            <li>To save workout you should fill the Name of training !</li>
                            <% } %>
                            <% if (request.getAttribute("legsEX_error") != null){%>
                            <li>To save workout you should fill the Name of exercise legs !</li>
                            <% } %>
                            <% if (request.getAttribute("forearmEX_error") != null){%>
                            <li>To save workout you should fill the Name of exercise forearm !</li>
                            <% } %>
                            <% if (request.getAttribute("bicepsEX_error") != null){%>
                            <li>To save workout you should fill the Name of exercise biceps !</li>
                            <% } %>
                                <% if (request.getAttribute("tricepsEX_error") != null){%>
                                <li>To save workout you should fill the Name of exercise triceps !</li>
                                <% } %>
                            <% if (request.getAttribute("backEX_error") != null){%>
                            <li>To save workout you should fill the Name of exercise back !</li>
                            <% } %>
                            <% if (request.getAttribute("chestEX_error") != null){%>
                            <li>To save workout you should fill the Name of exercise chest !</li>
                            <% } %>
                            <% if (request.getAttribute("absEX_error") != null){%>
                            <li>To save workout you should fill the Name of exercise abs !</li>
                            <% } %>
                            </ul>
                        </div>
                        <% } %>


                    <div class="inputField">
                        <label  class="inputLabel">Name of training </label>
                        <input class="form-control" name="training-name" type="text" placeholder="Here give a name of your train !" value="<%=request.getAttribute("training-name")%>"></input>
                    </div>

                        <table class="table table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Excercise</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true">Legs</i></span>
                                    <input class="form-control" placeholder="Legs" name="LegsEX" value="<%=request.getAttribute("LegsEX")%>" type="text">

                                </td>
                                <td>
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true">Forearms</i></span>
                                    <input class="form-control" placeholder="Forearms" name="ForearmEX" value="<%=request.getAttribute("ForearmEX")%>" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true">Biceps</i></span>
                                    <input class="form-control" placeholder="Biceps" name="BicepsEX" value="<%=request.getAttribute("BicepsEX")%>" type="text">
                                </td>
                                <td>
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true">Triceps</i></span>
                                    <input class="form-control" placeholder="Triceps" name="TricepsEX" value="<%=request.getAttribute("TricepsEX")%>" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true">Chest</i></span>
                                    <input class="form-control" placeholder="Chest" name="ChestEX" value="<%=request.getAttribute("ChestEX")%>" type="text">
                                </td>
                                <td>
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true">Belly(ABS)</i></span>
                                    <input class="form-control" placeholder="Belly" name="AbsEX" value="<%=request.getAttribute("AbsEX")%>" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true">Back</i></span>
                                    <input class="form-control" placeholder="Back" name="BackEX" value="<%=request.getAttribute("BackEX")%>" type="text">
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </table>
                        <input class="btn btn-success btn-block" type="submit" value="Save">
                        <input class="btn btn-danger btn-block" type="reset" value="Reset">

                </div>
            </div>

            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->


    </div>
    <!-- /#page-wrapper -->

    </form>
</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="bootstrap/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
