<%@ page import="com.google.gson.Gson" %>
<%@ page import="model.ActionTraining" %>
<%@ page import="model.Training" %>
<%@ page import="model.User" %>
<%@ page import="service.HistoryTrainingService" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Training Diary !</title>

    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- ChartJS -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/Chart.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/ts-chart-script.js"></script>


    <!-- Custom CSS -->
    <link href="bootstrap/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <%

        User user = (User) session.getAttribute("user");
        Training t = (Training) session.getAttribute("t");
        ActionTraining at = (ActionTraining) session.getAttribute("at");
    %>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/HomePage">Training Diary</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <%=user.getLogin()%> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile /TODO</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox /TODO</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings /TODO</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="/AddTraining"><i class="fa fa-fw fa-dashboard"></i> Add Training </a>
                </li>
                <li>
                    <a href="/editTraining"><i class="fa fa-fw fa-bar-chart-o"></i> Edit Training </a>
                </li>
                <li>
                    <a href="/history"><i class="fa fa-fw fa-wrench"></i> History of training</a>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo" class="collapse">
                        <li>
                            <a href="#">Dropdown Item</a>
                        </li>
                        <li>
                            <a href="#">Dropdown Item</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">

                    <form action="viewHistory" method="post">
                    <h1 class="page-header">
                        View History Page
                        <small>Welcome you <%= user.getFirstName() + " " + user.getLastName()%></small>
                    </h1>

                    <%
                        HistoryTrainingService historyTrainingService = new HistoryTrainingService();
                        int id = Integer.parseInt(request.getParameter("trainingId"));
                        request.getSession().setAttribute("id",id);
                        List<ActionTraining> atlList = historyTrainingService.getActionTrainingListById(id);
//                        ActionTraining [] tablica = atlList.toArray(new ActionTraining[atlList.size()]);

                        for (ActionTraining i : atlList) {

                            %>

                        <table class="table">
                            <thead>
                            <tr>
                                <th>Id Training<</th>
                                <th>ActionId Training</th>
                                <th>Value of ABS</th>
                                <th>Value of Back</th>
                                <th>Value of Triceps</th>
                                <th>Value of Chest</th>
                                <th>Value of Forearms</th>
                                <th>Value of Legs</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row"><%=i.getIdTraining()%></th>
                                <td><%=i.getIdActionTrain()%>  </td>
                                <td><%=i.getValueABS()%> kg </td>
                                <td><%=i.getValueBack()%> kg </td>
                                <td><%=i.getValueTriceps()%> kg </td>
                                <td><%=i.getValueChest()%> kg </td>
                                <td><%=i.getValueForearms()%> kg </td>
                                <td><%=i.getValueLegs()%> kg </td>
                            </tr>
                            </tbody>
                        </table>


                    <%

                        }
                    %>

                </div>
            </div>
            </form>
            <!-- /.row -->

            <canvas id="myChart" width="400" height="400"></canvas>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="bootstrap/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
